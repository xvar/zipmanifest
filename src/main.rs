use hyper::body::Buf;
use hyper::body::Bytes;
use hyper::header::RANGE;
use hyper::{Body, Client, Request};
use hyper_tls::HttpsConnector;
use reqwest::{Error, Method};
use std::io::{BufReader, Cursor, Read, Seek, SeekFrom, Write};
use std::time::Instant;
use zip::read::{find_content, make_reader, read_zipfile_from_stream, ZipFileReader};
use zip::spec::CentralDirectoryEnd;
use zip::types::ZipFileData;
use zip::write::FileOptions;
use zip::CompressionMethod;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    //create_test_zip();
    //let file = std::fs::File::open("C:\\temp\\test.zip").unwrap();
    //let mut reader = BufReader::new(file);

    let url = "https://github.com/veloren/veloren/releases/download/weekly/nightly-linux-aarch64-2022-03-05-14_22.zip";

    let eocd = get_eocd(url).await;
    println!("{:?}", eocd);

    let central_directory_bytes = get_central_directory_bytes(url, &eocd).await;
    let mut reader = Cursor::new(central_directory_bytes);

    let mut zip_files = Vec::<ZipFileData>::new();
    loop {
        if let Ok(data) = zip::read::central_header_to_zip_file(&mut reader, 0) {
            let local_header_len = 30 + data.file_name.as_bytes().len();
            println!(
                "Filename: {} Comment: {} Local Header Start: {} Local Header Length: {}",
                data.file_name, data.file_comment, data.header_start, local_header_len
            );
            zip_files.push(data);
        } else {
            // Ran out of headers
            break;
        }
    }

    // TODO: Determine which files we need based on comparing hashes in a local target directory
    // to those in the file comments, and request those files from the web server. Use contiguous
    // ranges to fetch multiple files in a single request where possible.
    /*for zip_file in zip_files {
        reader
            .seek(SeekFrom::Start(zip_file.header_start))
            .expect("Failed to seek");

        let mut file_reader = read_zipfile_from_stream(&mut reader)
            .expect("Failed to read zipfile from stream")
            .expect("There was no file?");

        let mut file_content = vec![];
        file_reader
            .read_to_end(&mut file_content)
            .expect("Failed to read file content");

        println!("{}", String::from_utf8_lossy(&file_content));
    }*/

    Ok(())
}

async fn get_eocd(url: &str) -> CentralDirectoryEnd {
    let max_eocd_size: u32 = u16::MAX as u32 + 22;
    let client = reqwest::Client::new();

    println!("Requesting EOCD...");
    let start_time = Instant::now();
    let req = client
        .get(url)
        .header("Range", format!("bytes=-{}", max_eocd_size))
        .send()
        .await
        .unwrap();

    let content = req.bytes().await.unwrap();
    println!(
        "Got EOCD, request took: {}ms",
        (Instant::now() - start_time).as_millis()
    );
    let mut reader = Cursor::new(content);

    let start_time = Instant::now();
    let eocd = CentralDirectoryEnd::find_and_parse(&mut reader).unwrap().0;
    println!(
        "EOCD parsing took {}ms",
        (Instant::now() - start_time).as_millis()
    );
    eocd
}

async fn get_central_directory_bytes(url: &str, eocd: &CentralDirectoryEnd) -> Bytes {
    let client = reqwest::Client::new();

    println!("Requesting Central Directory...");
    let start_time = Instant::now();
    let start = eocd.central_directory_offset;
    let end = eocd.central_directory_offset + eocd.central_directory_size;
    let req = client
        .get(url)
        .header("Range", format!("bytes={}-{}", start, end))
        .send()
        .await
        .unwrap();
    let content = req.bytes().await.unwrap();
    println!(
        "Got Central Directory, request took: {}ms",
        (Instant::now() - start_time).as_millis()
    );
    content
}

#[allow(dead_code)]
fn create_test_zip() {
    let path = std::path::Path::new("C:\\temp\\test.zip");
    let file = std::fs::File::create(&path).unwrap();

    let mut zip = zip::ZipWriter::new(file);

    let options = FileOptions::default().compression_method(zip::CompressionMethod::Deflated);
    zip.start_file("file1.txt", options).unwrap();
    zip.write_all(b"Hello, World! This is file1\n").unwrap();
    zip.set_file_comment("84944be7e754d889921dd51e5fd51c89")
        .unwrap();

    zip.start_file("file2.txt", Default::default()).unwrap();
    zip.write_all(b"This is file 2!\nIt has 2 lines in it")
        .unwrap();
    zip.set_file_comment("99cb3fcf1593ee101f205fcb982f9929")
        .unwrap();

    zip.finish().unwrap();
}
